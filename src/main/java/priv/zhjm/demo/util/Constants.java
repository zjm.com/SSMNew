package priv.zhjm.demo.util;

public class Constants {

	public static final String NO_INTERCEPTOR_PATH = ".*/((login)|(logout)|(mobile)|(druid/*)).*"; // 不对匹配该值的访问路径拦截（正则）
	public static final Object SESSION_USER = "sessionUser";
	public static final String SESSION_allmenuList = "allmenuList"; // 全部菜单
	public static final String SESSION_QX = "QX";
	public static final String SESSION_USERNAME = "USERNAME"; // 用户名
	public static final String LOGIN = "/login_toLogin";
}
