package priv.zhjm.demo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository("daoSupport")
public class DaoSupport implements DAO {

	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	/**
	 * 保存对象
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object save(String str, Object obj) throws Exception {
		return sqlSessionTemplate.insert(str, obj);
	}

	/**
	 * 批量更新
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object batchSave(String str, List<?> objs) throws Exception {
		return sqlSessionTemplate.insert(str, objs);
	}

	/**
	 * 修改对象
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object update(String str, Object obj) throws Exception {
		return sqlSessionTemplate.update(str, obj);
	}

	/**
	 * 批量更新
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public void batchUpdate(String str, List<?> objs) throws Exception {
		SqlSessionFactory sqlSessionFactory = sqlSessionTemplate.getSqlSessionFactory();
		// 批量执行器
		SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
		try {
			if (objs != null) {
				for (int i = 0, size = objs.size(); i < size; i++) {
					sqlSession.update(str, objs.get(i));
				}
				sqlSession.flushStatements();
				sqlSession.commit();
				sqlSession.clearCache();
			}
		} finally {
			sqlSession.close();
		}
	}

	/**
	 * 批量删除
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object batchDelete(String str, List<?> objs) throws Exception {
		return sqlSessionTemplate.delete(str, objs);
	}

	/**
	 * 删除对象
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object delete(String str, Object obj) throws Exception {
		return sqlSessionTemplate.delete(str, obj);
	}

	/**
	 * 查找对象
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object findForObject(String str, Object obj) throws Exception {
		return sqlSessionTemplate.selectOne(str, obj);
	}

	/**
	 * 查找对象
	 * 
	 * @param str
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Object findForList(String str, Object obj) throws Exception {
		return sqlSessionTemplate.selectList(str, obj);
	}

	public Object findForMap(String str, Object obj, String key, String value) throws Exception {
		return sqlSessionTemplate.selectMap(str, obj, key);
	}

	/**
	 * 根据传入的查询sql语句返回可以向<select></select>的<option>项中添加项的字符串
	 * 
	 * @param sql
	 *            查询sql语句
	 * @param display
	 * @param value
	 * @return
	 */
	public String getList(String sql, String display, String value) {
		SqlSession session = null;
		try {
			String s3 = "";
			session = sqlSessionTemplate.getSqlSessionFactory().openSession();
			Connection conn = session.getConnection();
			System.out.println(conn + ">>>.");
			Statement statement = conn.createStatement();
			ResultSet resultset;
			for (resultset = statement.executeQuery(sql); resultset.next();) {
				String displayvalue = resultset.getString(display);
				if (displayvalue == null)
					displayvalue = "";
				s3 = s3 + "<option value=\"" + resultset.getString(value) + "\">" + displayvalue + "</option>";
			}

			statement.close();
			resultset.close();
			return s3;
		} catch (Exception exception) {
			exception.printStackTrace();
			return "<option>Error</option>";
		} finally {
		}
	}

	/**
	 * 
	 * @param sql
	 * @param display
	 * @param value
	 * @param realValue
	 * @return
	 */
	public String getList(String sql, String display, String value, String realValue) {
		SqlSession session = null;
		try {
			String s4 = "";
			session = sqlSessionTemplate.getSqlSessionFactory().openSession();
			Connection conn = session.getConnection();
			Statement statement = conn.createStatement();
			ResultSet resultset;
			for (resultset = statement.executeQuery(sql); resultset.next();) {
				String s6 = resultset.getString(value);
				String displayvalue = resultset.getString(display);
				if (displayvalue == null)
					displayvalue = "";

				if (s6.equals(realValue))
					s4 = s4 + "<option value=\"" + s6 + "\" selected>" + displayvalue + "</option>";
				else
					s4 = s4 + "<option value=\"" + s6 + "\">" + displayvalue + "</option>";
			}
			statement.close();
			resultset.close();
			return s4;
		} catch (Exception exception) {
			return "<option>Error</option>";
		} finally {
			session.close();
		}
	}

	/**
	 * 根据传入的查询sql语句返回可以向<select></select>的
	 * <option>项中添加项的字符串(html片段),并根据传入的值selected
	 * 
	 * @param sql
	 * @param display:显示的值
	 * @param value：实际值
	 * @param realValue：缺省值
	 * @return 例如：str="select id ,name from table" 调用getList(str,name,id,"001")
	 *         返回：<option value="001" selected>name1</option>
	 *         <option value="002" >name2</option>
	 *         <option value="003" >name3</option> ...
	 */
	public String getList(String sql, String display, String value, List<?> realValue) {
		SqlSession session = null;
		try {
			String s4 = "";
			session = sqlSessionTemplate.getSqlSessionFactory().openSession();
			Connection conn = session.getConnection();
			Statement statement = conn.createStatement();
			ResultSet resultset;
			for (resultset = statement.executeQuery(sql); resultset.next();) {
				String s6 = resultset.getString(value);
				String displayvalue = resultset.getString(display);
				if (displayvalue == null)
					displayvalue = "";
				if (null != realValue && realValue.size() > 0) {
					if (realValue.contains(s6)) {
						s4 = s4 + "<option value=\"" + s6 + "\" selected>" + displayvalue + "</option>";
					} else {
						s4 = s4 + "<option value=\"" + s6 + "\">" + displayvalue + "</option>";
					}
				} else {
					s4 = s4 + "<option value=\"" + s6 + "\">" + displayvalue + "</option>";
				}
			}
			statement.close();
			resultset.close();
			return s4;
		} catch (Exception exception) {
			return "<option>Error</option>";
		} finally {
			session.close();
		}
	}

	public String getValue(String s) {
		String s1 = "";
		Statement statement = null;
		SqlSession session = null;
		try {
			session = sqlSessionTemplate.getSqlSessionFactory().openSession();
			Connection conn = session.getConnection();
			statement = conn.createStatement();
			ResultSet resultset = statement.executeQuery(s);
			if (resultset.next())
				s1 = resultset.getString(1) == null ? "" : resultset.getString(1);
			else
				s1 = "";
			resultset.close();
			statement.close();
		} catch (Exception exception) {
		} finally {
			session.close();
		}
		return s1;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public int getIntValue(String s) {
		int int1 = 0;
		Statement statement = null;
		SqlSession session = null;
		try {
			session = sqlSessionTemplate.getSqlSessionFactory().openSession();
			Connection conn = session.getConnection();
			statement = conn.createStatement();
			ResultSet resultset = statement.executeQuery(s);
			if (resultset.next())
				int1 = resultset.getInt(1);
			resultset.close();
			statement.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			session.close();
		}
		return int1;
	}
}
