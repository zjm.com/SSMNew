package priv.zhjm.demo.entity;
/**
 * 
 * @author zhjm01
 * @date 2015年11月6日
 */
public class Role {
	private String id;
	private String name;
	private String right;
	private String parentId;
	private String addQx;
	private String delQx;
	private String editQx;
	private String chaQx;
	private String qxId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRight() {
		return right;
	}
	public void setRight(String right) {
		this.right = right;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getAddQx() {
		return addQx;
	}
	public void setAddQx(String addQx) {
		this.addQx = addQx;
	}
	public String getDelQx() {
		return delQx;
	}
	public void setDelQx(String delQx) {
		this.delQx = delQx;
	}
	public String getEditQx() {
		return editQx;
	}
	public void setEditQx(String editQx) {
		this.editQx = editQx;
	}
	public String getChaQx() {
		return chaQx;
	}
	public void setChaQx(String chaQx) {
		this.chaQx = chaQx;
	}
	public String getQxId() {
		return qxId;
	}
	public void setQxId(String qxId) {
		this.qxId = qxId;
	}
}
