package priv.zhjm.demo.controller.system;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import priv.zhjm.demo.controller.base.BaseController;
import priv.zhjm.demo.entity.User;
import priv.zhjm.demo.util.Constants;

@Controller
public class MainController extends BaseController {

	@RequestMapping("/index")
	public String index() {
		return "/index";
	}
	
	@RequestMapping("/login_login")
	public String login(String username,String password) {
		//shiro管理的session
		Subject currentUser = SecurityUtils.getSubject();  
		Session session = currentUser.getSession();
		
		User user = new User();
		user.setUserName(username);
		user.setPassWord(password);
		
		session.setAttribute(Constants.SESSION_USER, user);
		//shiro加入身份验证
		Subject subject = SecurityUtils.getSubject(); 
	    UsernamePasswordToken token = new UsernamePasswordToken(username, password); 
	    try { 
	        subject.login(token);
	        System.out.println("----------->3true");
	    } catch (AuthenticationException e) { 
	    	e.printStackTrace();
	    }
		return "/index";
	}
	
	@RequestMapping("/login_toLogin")
	public String to_login() {
		return "/login";
	}
}
