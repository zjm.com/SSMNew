package priv.zhjm.demo.controller.mobile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import priv.zhjm.demo.controller.base.BaseController;
import priv.zhjm.demo.entity.Page;
import priv.zhjm.demo.service.CustomerService;
import priv.zhjm.demo.util.PageData;
import priv.zhjm.demo.util.Tools;

@Controller
@RequestMapping(value = "/mobile", produces = { "application/json;charset=UTF-8" })
public class MobileController extends BaseController {
	
	@Resource
	CustomerService customerService;

	@RequestMapping(value = "/getCustomerList")
	public @ResponseBody Map<String, Object> getCustomerList(Page page) throws Exception {
		PageData pd = new PageData();
		Map<String, Object> map = new HashMap<String, Object>();
		logger.info("logger ----> getCustomerList");
		map.put("interface", "mobile/getCustomerList");
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			pd = this.getPageData();
			pd.put("DELETED", "0");
			page.setPd(pd);
			// 获取大类下商店列表
			List<PageData> varList = customerService.list(page);
			if (varList != null && varList.size() > 0) {
				for (PageData p : varList) {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					dataMap.put("id", Tools.isNull(p.get("uuid")));
					dataMap.put("name", Tools.isNull(p.get("UNAME")));
					dataMap.put("pword", Tools.isNull(p.get("PWORD")));
					dataMap.put("nickname", Tools.isNull(p.get("NICKNAME")));
					dataMap.put("sex", Tools.isNull(p.get("SEX")));
					dataMap.put("score", Tools.isIntNull(p.get("SCORE")));

					dataList.add(dataMap);
				}
				map.put("status", "success");
			} else {
				map.put("status", "nodata");
			}
			map.put("dataList", dataList);
			map.put("totalPage", page.getTotalPage());
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", "failture");
			logger.error(e.toString(), e);
		}
		return map;
	}
}
